function calcAverage(score1, score2, score3, score4) {
    let avg = (score1 + score2 + score3 + score4) / 4;

    if (avg <= 74) {
        console.log(
            'Hello, student, your average is ' + avg + '. The letter equivalent is F'
        );
    } else if (avg >= 75 && avg <= 79) {
        console.log(
            'Hello, student, your average is' + avg + '. The letter equivalent is D'
        );
    } else if (avg >= 80 && avg <= 84) {
        console.log(
            'Hello, student, your average is ' + avg + '. The letter equivalent is C'
        );
    } else if (avg >= 85 && avg <= 89) {
        console.log(
            'Hello, student, your average is ' + avg + '. The letter equivalent is B'
        );
    } else if (avg >= 90 && avg <= 95) {
        console.log(
            'Hello, student, your average is ' + avg + '. The letter equivalent is A'
        );
    } else if (avg > 96) {
        console.log(
            'Hello, student, your average is ' + avg + '. The letter equivalent is A+'
        );
    } else {
        console.log('Error');
    }
}

calcAverage(70, 70, 72, 71);